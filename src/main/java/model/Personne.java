package model;

public class Personne {


    private int id;
    private String name;
    private int age;
    private int numFriends;


    public Personne(){}

    public Personne(int id, String name, int age, int numFriends) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.numFriends = numFriends;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNumFriends() {
        return numFriends;
    }

    public void setNumFriends(int numFriends) {
        this.numFriends = numFriends;
    }
}
