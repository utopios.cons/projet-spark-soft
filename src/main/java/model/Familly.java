package model;

public class Familly {


    private Integer id;
    private String nom;
    private Integer age;


    public Familly(Integer id, String nom, Integer age) {
        this.id = id;
        this.nom = nom;
        this.age = age;
    }


    public Familly(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
