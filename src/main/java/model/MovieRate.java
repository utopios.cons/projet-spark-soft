package model;

public class MovieRate {

    private int movieId;
    private int rate;


    public MovieRate(){}

    public MovieRate(int movieId, int rate) {
        this.movieId = movieId;
        this.rate = rate;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
