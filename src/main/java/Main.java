import model.*;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.util.LongAccumulator;
import tools.ReadFromFile;
import tools.SparkContext;

import org.javatuples.Triplet;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static org.apache.spark.sql.functions.*;


public class Main {

    public static void main(String[] args) throws AnalysisException, TimeoutException, StreamingQueryException {


        SparkSession session = SparkContext.getSession();
        JavaSparkContext session1 = SparkContext.getContext();

        StructType type = new StructType()
                .add("id", DataTypes.IntegerType, false)
                .add("name", DataTypes.StringType, false)
                .add("age", DataTypes.IntegerType, false)
                .add("numFriends", DataTypes.IntegerType, false);


        // Dataset<Personne> ds = session.read().option("header", true).option("inferSchema", true).csv("data-demo/friends.csv").as(Encoders.bean(Personne.class));
        // Dataset<Personne> ds = session.read().schema(type).csv("data-demo/friends.csv").as(Encoders.bean(Personne.class));
        //ds.createGlobalTempView("personnes");
        //  ds.createOrReplaceTempView("personnes");
        //session.sql("SELECT * from global_temp.personnes where age > 20 and age <25").show();
        //  session.sql("SELECT * from personnes where age > 20 and age <25").show();

        // ds.groupBy("age").count().show();


        // ds.groupBy("age").agg(avg("numFriends").alias("Nombre moyen ami")).sort("age").show();


       /* Triplet<Integer, String, Integer> fam = Triplet.with(1, "Jean", 35);
        Triplet<Integer, String, Integer> fam1 = Triplet.with(2, "Pierre", 45);
        Triplet<Integer, String, Integer> fam2 = Triplet.with(3, "Marc", 90);
        Triplet<Integer, String, Integer> fam3 = Triplet.with(4, "Sylvain", 80);

        List<Triplet> liste = new ArrayList<>(Arrays.asList(fam, fam1, fam2, fam3));

        JavaRDD<Familly>rdd1 = session1.parallelize(liste).map(f -> {
            return new Familly((Integer) f.getValue(0), (String) f.getValue(1), (Integer) f.getValue(2));
        });

        Dataset<Familly> famillYDS = session.sqlContext().createDataset(rdd1.rdd(), Encoders.bean(Familly.class));

        famillYDS.printSchema();
        famillYDS.show();*/


       /* Dataset dsMySql = session.sqlContext().read().format("jdbc")
                .option("url", "jdbc:mysql://ddb-spark.ctqpsfysnuso.eu-west-3.rds.amazonaws.com/ddb_spark")
                .option("dbtable", "client")
                .option("user", "admin")
                .option("password", "rootroot")
                .load();

        dsMySql.show();

        dsMySql.createOrReplaceTempView("client");

         session.sql("SELECT * FROM client where client.id_clt = 3").show();*/

        //UDF  : Convertir les noms de la colonne name dans le fichier friends dans une nouvelle colonne
        // Dataset<Personne> ds3 = session.read().schema(type).csv("data-demo/friends.csv").as(Encoders.bean(Personne.class));
        // session.sqlContext().udf().register("toUpper", (UDF1<String, String>) (e) -> e.toUpperCase(), DataTypes.StringType);
        //    session.sqlContext().udf().register("toUpper", toUpper, DataTypes.StringType);
        //  ds3.withColumn("nameUpper", callUDF("toUpper", col("name"))).show();


        // TP tri film par rate avec comme infos (id + rate + nom) : version 1
        //    JavaRDD<MovieRate> rddRate = session1.textFile("data-demo/film.data").map(p -> {
        //     String[] data = p.split("\t");
        // return new MovieRate(new Integer(data[1]), new Integer(data[2]));

        // });

        // Dataset<MovieRate> movieRateDataset = session.sqlContext().createDataset(rddRate.rdd(), Encoders.bean(MovieRate.class));

       /* JavaRDD<MovieName> rddName = session1.textFile("data-demo/names.item").map(p -> {
            String[] data = p.split(",");
            return new MovieName(new Integer(data[0]), data[1]);

        });

        Dataset<MovieName> movieNameDataset = session.sqlContext().createDataset(rddName.rdd(), Encoders.bean(MovieName.class));

        Dataset join = movieRateDataset.groupBy("movieId").agg(max("rate").alias("rate")).join(movieNameDataset, "movieId")
                .sort(col("rate").desc());

        join.show(100);*/


        // variable de broadcast :
       /* Broadcast<String> br = session1.broadcast("prefix");
        session.sqlContext().udf().register("withBroadCast", (UDF1<String, String>) (e) -> br.getValue() + " " + e.toString(), DataTypes.StringType);
        ds3.withColumn("name_broadcast", callUDF("withBroadCast", col("name"))).show();*/


        // TP tri film par rate avec comme infos (id + rate + nom) : version 2 avec Broadcast
        /*Broadcast<Map<Integer, String>> mapBroadcastFilmName = session1.broadcast(ReadFromFile.getMapFilmsName());
        session.sqlContext().udf().register("filmName", (UDF1<Integer, String>) e -> mapBroadcastFilmName
                .getValue().get(new Integer(e.toString())), DataTypes.StringType);
        movieRateDataset.groupBy("movieId").agg(max("rate").alias("rate"))
                .withColumn("Name", callUDF("filmName", col("movieId")))
                .sort(col("rate").desc()).show();*/


        /*Map herosName = ReadFromFile.getSuperHeroName();

        Broadcast<Map<Integer, String>> mapBroadcastHero = session1.broadcast(herosName);
        session.sqlContext().udf().register("heroName", (UDF1<Integer, String>) e -> mapBroadcastHero
                .getValue().get(new Integer(e.toString())), DataTypes.StringType);


        Dataset<SuperHeroConnect> superHeroConnectDataset = session.read().text("data-demo/Marvel-graph.txt").as(Encoders.bean(SuperHeroConnect.class));
    Dataset det = superHeroConnectDataset.withColumn("id", split(col("value"), " ").getItem(0))
                .withColumn("connection", size(split(col("value"), " ")))
                .groupBy("id").agg(sum("connection")
                        .alias("connection")).withColumn("name", callUDF("heroName", col("id")))
                .sort(col("connection").desc()).cache();

            Row most = (Row) det.first();
*/


       /* StructType superHeroSchema = new StructType().add("id", DataTypes.IntegerType, false)
                .add("name", DataTypes.StringType, false);

        Dataset<SuperHeroName> names = session.read()
                .schema(superHeroSchema)
                .option("sep", " ")
                .csv("data-demo/Marvel-names.txt")
                .as(Encoders.bean(SuperHeroName.class));

        Dataset<SuperHero> lines = session.read().text("data-demo/Marvel-graph.txt").as(Encoders.bean(SuperHero.class));

        Dataset connections = lines.withColumn("id", split(col("value"), " ").getItem(0))
                .withColumn("connections", size(split(col("value"), " ")))
                .groupBy("id").agg(sum("connections").alias("connections"));

        Row mostPopular = (Row) connections.sort(col("connections").desc()).first();

        Object mostPopularName = names.filter((FilterFunction<SuperHeroName>) e -> e.getId() == new Integer(mostPopular.getString(0))).collect();
*/



      /*  Dataset s = session.readStream().format("rate").option("rowPersecond", 1).load().withColumn("result",col("value"));


        try{

            s.writeStream().outputMode("update").format("console").start().awaitTermination();


        }catch (Exception e){

        }*/


        /*StructType types = new StructType().add("id", DataTypes.IntegerType, false)
                .add("name", DataTypes.StringType, false)
                .add("age", DataTypes.IntegerType, false)
                .add("numFriends", DataTypes.IntegerType, true);

        // Input
        Dataset <Personne> personneDataset = session.readStream().format("csv").option("header", true)
                .option("path", "data")
                .schema(types)
                .load().as(Encoders.bean(Personne.class));

        // Output
        personneDataset.writeStream().format("console").outputMode("update").start().awaitTermination();*/


        Dataset <Row> lines = session.readStream().format("socket").option("host", "localhtotost")
                .option("port",9999)
                .load();


        Dataset <String> words = lines.as(Encoders.STRING())
                .flatMap((FlatMapFunction<String, String>) x -> Arrays.asList(x.split(" "))
                        .iterator(), Encoders.STRING());

        Dataset <Row> wordounts = words.groupBy("value").count();


        StreamingQuery query = wordounts.writeStream().outputMode("complete").format("console").start();

        query.awaitTermination();


    }


    private static UDF1 toUpper = new UDF1<String, String>() {

        public String call(String element) {

            return element.toUpperCase();

        }
    };


}