package tools;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class SparkContext {


    private static final SparkConf conf = new SparkConf().setAppName("local-app").setMaster("local[*]");

    private static final JavaSparkContext context = createContext();

    private static final SparkSession session = createSession();


    public static JavaSparkContext getContext(){
        return context;
    }

    public  static  SparkSession getSession(){
        return session;
    }


    private static JavaSparkContext createContext(){
        JavaSparkContext context = new JavaSparkContext(conf);

        return context;

    }

    private static SparkSession createSession(){
        return SparkSession.builder().appName("local-app").master("local[*]").getOrCreate();
    }



}
