package tools;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ReadFromFile {


    public static Map<Integer, String> getMapFilmsName() {

        Map<Integer, String> result = new HashMap<>();
        try {
            File file = new File("data-demo/names.item");
            Scanner reader = new Scanner(file);

            while (reader.hasNextLine()) {

                String[] data = reader.nextLine().split(",");
                result.put(Integer.valueOf(data[0]), data[1]);

            }

        } catch (Exception e) {

        }

        return result;
    }


    public static Map<Integer, String> getSuperHeroName() {

        Map<Integer, String> result2 = new HashMap<>();
        try {
            File file = new File("data-demo/Marvel-names.txt");
            Scanner reader = new Scanner(file);

            while (reader.hasNextLine()) {

                String[] data = reader.nextLine().split("\"");
                result2.put(Integer.valueOf(data[0]), data[1].replace("\"", ""));

            }

        } catch (Exception e) {

        }

        return result2;
    }







}
